#include "Form.h"
class Rectangle:public Form 
{
protected:
        int m_width;
        int m_height;
public:
        Rectangle(string name, int width, int height);
        ~Rectangle();
        void setWidth(int width);
        int getWidth();
        void showMessage();
        int getArea();
        int getPerimeter();
};