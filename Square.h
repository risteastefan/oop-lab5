#include "Rectangle.h"

class Square:public Form 
{
protected:
        int m_side;
public:
        Square(string name, int side);
        ~Square();
        void setSide(int side);
        int getSide();
        void showMessage();
        int getPerimeter();
        int getArea();
};