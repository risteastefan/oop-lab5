// Lab5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Square.h"


int main()
{
        Form *form = new Form("forma1");
        cout<<"forma: "<<form->getName()<<endl;
        form->showMessage();

        Rectangle *rect = new Rectangle("rectangle 1", 1, 2);
        cout<<"rectangle name: "<<rect->getName()<<" width:"<<rect->getWidth()<<endl;
        rect->showMessage();

        Square *square = new Square("square 1", 20);
        cout<<"square name: "<<square->getName()<<" side:"<<square->getSide()<<endl;
        square->showMessage();
        
        cout<<form->getArea()<<"\n";
        cout<<rect->getArea()<<"\n";
        cout<<square->getArea()<<"\n";

        cout<<form->getPerimeter()<<"\n";
        cout<<rect->getPerimeter()<<"\n";
        cout<<square->getPerimeter()<<"\n";

        delete form;
        delete rect;
        delete square;
        return 0;
}