#include "iostream"
#include "string"
using namespace std;
class Form 
{
private:
        string m_color;
protected:
        string m_name;
        void setColor(string color);
public:
        Form(string name);
        ~Form();
        string getName();
        void setName(string name);
        int getArea();
        int getPerimeter();
        void showMessage();
};